﻿Shader "Sprites/SimpleLighter"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_LightSharpness ("LightSharpness", Range(1, 5)) = 3
	}
	SubShader
	{
		Tags 
		{ 
			"Queue"="Transparent"
		}

        Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				half4 vertex : POSITION;
				half4 color : COLOR;
				half2 uv : TEXCOORD0;
			};

			struct v2f
			{
				half4 vertex : SV_POSITION;
				half2 uv : TEXCOORD0;
				half4 color : TEXCOORD1;
				half2 screenPos:TEXCOORD2;
			};

			sampler2D _MainTex;
			half4 _MainTex_ST;
			half _LightSharpness;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.color = v.color;
				o.screenPos = ComputeScreenPos(o.vertex);

				return o;
			}
			
			half4 frag (v2f i) : SV_Target
			{
				half4 col = tex2D(_MainTex, i.uv)* i.color;
				half border = abs(i.screenPos.y - 0.5) * 2;
				border = 1 - border;
				border = clamp(border * _LightSharpness, 0, 1);
				border = 1 - border;

				col.rgb = lerp(col.rgb, 1, border);

				return col;
			}
			ENDCG
		}
	}
}
