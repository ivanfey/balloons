﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core
{
    public class BalloonController : MonoBehaviour
    {
        public static float MainGameTimer = 0f;
        public static int MainGameScore = 0;
        public static UnityEvent OnGameDone = new UnityEvent();
        public static UnityEvent OnScoreChanged = new UnityEvent();

        private List<Balloon> _balloonList = new List<Balloon>();

        [SerializeField]
        private GameObject _defaultBalloon = null;
        [SerializeField]
        private float _gameTime = 60f;
        [SerializeField]
        private float _spawnPeriod = 1.25f;
        [SerializeField]
        private float _extraSpeedGain = 0f;

        private void Awake()
        {
            MainGameTimer = 0f;
            MainGameScore = 0;
        }

        private IEnumerator Start()
        {
            while (MainGameTimer <= _gameTime)
            {
                yield return new WaitForSeconds(_spawnPeriod);

                SpawnBalloon();
            }
        }

        private void Update()
        {
            if (MainGameTimer > _gameTime)
                return;

            var deltaTime = Time.deltaTime;

            for (int i = 0; i < _balloonList.Count; i++)
            {
                _balloonList[i].Move(deltaTime, (MainGameTimer / _gameTime) * _extraSpeedGain);
            }

            MainGameTimer += deltaTime;
            if (MainGameTimer > _gameTime)
                OnGameDone.Invoke();
        }

        private void SpawnBalloon()
        {
            for (int i = 0; i < _balloonList.Count; i++)
            {
                if (_balloonList[i].gameObject.activeSelf)
                    continue;

                ActivateBalloon(_balloonList[i]);
                return;
            }

            var newBalloonGO = Instantiate(_defaultBalloon, _defaultBalloon.transform.parent);
            var newBalloon = newBalloonGO.GetComponent<Balloon>();
            _balloonList.Add(newBalloon);

            ActivateBalloon(newBalloon);
        }

        private void ActivateBalloon(Balloon balloon)
        {
            balloon.gameObject.SetActive(true);
            balloon.Init(BurstBalloon, DespawnBalloon);
        }

        private void BurstBalloon(Balloon balloon)
        {
            if (MainGameTimer > _gameTime)
                return;

            MainGameScore += balloon.GetScore();
            OnScoreChanged.Invoke();
            DespawnBalloon(balloon);
        }

        private void DespawnBalloon(Balloon balloon)
        {
            balloon.gameObject.SetActive(false);
        }
    }
}

