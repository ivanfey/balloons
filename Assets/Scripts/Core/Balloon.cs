﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Core
{
    public class Balloon : MonoBehaviour
    {
        [System.Serializable]
        private class BalloonType
        {
            [SerializeField]
            private string _name = "default balloon";
            [SerializeField]
            private Color _color = Color.white;
            [SerializeField]
            private float _size = 150f;
            [SerializeField]
            private float _speed = 15f;
            [SerializeField]
            private int _score = 2;

            public Color GetColor()
            {
                return _color;
            }

            public float GetSize()
            {
                return _size;
            }

            public float GetSpeed()
            {
                return _speed;
            }

            public int GetScore()
            {
                return _score;
            }

            public Vector3 GetRandomSpawnPoint()
            {
                var halfSize = _size * 0.5f;
                var spawnPoint = Vector3.zero;
                spawnPoint.x = Random.Range(halfSize, Screen.width - halfSize);
                spawnPoint.y = -halfSize;

                return spawnPoint;
            }

            public float GetDespawnHeight()
            {
                return Screen.height + _size * 0.5f;
            }
        }

        [System.Serializable]
        private class BalloonEvent : UnityEvent<Balloon> { }

        private BalloonEvent _onBursted = new BalloonEvent();
        private BalloonEvent _onDespawned = new BalloonEvent();

        [SerializeField]
        private BalloonType[] _balloonTypes = new BalloonType[0];

        [SerializeField]
        private Image _mainImage = null;
        [SerializeField]
        private RectTransform _rectTrans = null;
        [SerializeField]
        private Button _burstButton = null;

        private BalloonType _balloonType = null;

        public void Init(UnityAction<Balloon> onBurst, UnityAction<Balloon> onDespawn)
        {
            _balloonType = _balloonTypes[Random.Range(0, _balloonTypes.Length)];

            _mainImage.color = _balloonType.GetColor();
            _rectTrans.sizeDelta = Vector2.one * _balloonType.GetSize();

            _rectTrans.position = _balloonType.GetRandomSpawnPoint();

            _burstButton.onClick.RemoveAllListeners();
            _burstButton.onClick.AddListener(Burst);

            _onBursted.RemoveAllListeners();
            if(onBurst != null)
                _onBursted.AddListener(onBurst);
            _onDespawned.RemoveAllListeners();
            if (onDespawn != null)
                _onDespawned.AddListener(onDespawn);
        }

        public void Move(float deltaTime, float extraSpeed)
        {
            var baseSpeed = _balloonType.GetSpeed();
            var deltaPos = Vector3.up * (baseSpeed + extraSpeed) * deltaTime;

            _rectTrans.position += deltaPos;

            if (_rectTrans.position.y > _balloonType.GetDespawnHeight())
                _onDespawned.Invoke(this);
        }

        private void Burst()
        {
            _onBursted.Invoke(this);
        }

        public int GetScore()
        {
            return _balloonType.GetScore();
        }
    }
}