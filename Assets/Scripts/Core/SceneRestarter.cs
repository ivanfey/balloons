﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core
{
    public class SceneRestarter : MonoBehaviour
    {
        public void Restart()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
        }
    }
}

