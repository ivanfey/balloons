﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class ScoreShower : MonoBehaviour
    {
        [SerializeField]
        private Text _scoreText = null;

        private void OnEnable()
        {
            Core.BalloonController.OnScoreChanged.AddListener(ChangeScore);
            ChangeScore();
        }

        private void OnDisable()
        {
            Core.BalloonController.OnScoreChanged.RemoveListener(ChangeScore);
        }

        private void ChangeScore()
        {
            _scoreText.text = Core.BalloonController.MainGameScore.ToString();
        }
    }
}