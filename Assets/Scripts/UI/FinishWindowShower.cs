﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class FinishWindowShower : MonoBehaviour
    {
        [SerializeField]
        private GameObject _finishWindowRootGO = null;
        [SerializeField]
        private float _showTime = 0.75f;
        [SerializeField]
        private CanvasGroup _group = null;
        [SerializeField]
        private Text _scoreText = null;

        private void OnEnable()
        {
            Core.BalloonController.OnGameDone.AddListener(Show);
        }

        private void OnDisable()
        {
            Core.BalloonController.OnGameDone.RemoveListener(Show);
        }

        private void Show()
        {
            if (_showing != null)
                return;

            _showing = Showing();

            StartCoroutine(_showing);
        }

        private IEnumerator _showing = null;
        private IEnumerator Showing()
        {
            _scoreText.text = "Your score: " + Core.BalloonController.MainGameScore;

            _finishWindowRootGO.gameObject.SetActive(true);

            var timer = 0f;
            while (timer <= _showTime)
            {
                timer += Time.deltaTime;

                _group.alpha = timer / _showTime;

                yield return null;
            }

            _group.interactable = true;

            _showing = null;
        }
    }
}