﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class TimerShower : MonoBehaviour
    {
        [SerializeField]
        private Text _timerText = null;

        private void Update()
        {
            _timerText.text = System.Math.Round(Core.BalloonController.MainGameTimer, 2).ToString();
        }
    }
}

